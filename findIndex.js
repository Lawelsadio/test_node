// pour lancer en ligne de commande: node findIndex.js 1,0,0,1,0,1 
// pour lancer en ligne de commande: node findIndex.js 1,0,0,1,0,1 

//fonction permettant de trouver la plus longue sequence de 1 en remplaçant un zero dans un tableau
function findIndex(tab) {
 // let tab =[1,0,1,1,1,0,1,1,1,1,0,1,1,1,1,0,0,1,1]   
  let idx0 = -1
  let count1 = 0
  let maxCount = {
    index: 0,
    count1: 0
  }
  tab.forEach((val, index) => {
   // console.log(val,index);
    if (val == 0) {
        if (count1 >= maxCount.count1) {
            maxCount = {
              index: idx0,
              count1
            }
          }
  // console.log(maxCount)
        count1 = tab[index - 1] == 1 ? index - idx0 : 1
        idx0 = index
    } else {
        count1++
    }
  })
  if (count1 >= maxCount.count1) {
        maxCount = {
          index: idx0,
          count1
        }
    }
  //console.log("l'index est:",maxCount.index)
  return console.log('dans le tableau',tab, 'index converti est le numero :',maxCount.index)
}

// Recuperation des paramètres
const myArgs = process.argv.slice(2);
if (myArgs.length===0){
  console.log("veillez ajouter des arguments tels que 1,0,0,1,0,1 ");
}
else{
  // création du tableau 
const tab = myArgs[0].split(',');

// affichage de l'index
findIndex(tab)
  
}


